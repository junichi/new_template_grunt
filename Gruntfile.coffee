###*
  Gruntfile.coffee

  version 5.1.0
  update 12/04/14

  Copyright (c) 2014 Kentaro Otsuka, Junichi Honda All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  ・Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.

  ・Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

  ・Neither the name of the MONSTER-DIVE nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###
module.exports = (grunt) ->
  'use strict'
  MONSTER = require("./MONSTER.js")
  _sassFilesArr = []
  _releaseFilesArr = []
  #_browserSyncPath = undefined
  #_setSassCompile = undefined
  _browserSyncPath = (if(not MONSTER.isDebug) then MONSTER.path.release else MONSTER.path.bin)
  _pkg = grunt.file.readJSON 'package.json'

  # --------------------------------------------------------------
  # コンパイルするSassファイルをSassタスクに追加
  # --------------------------------------------------------------
  do ->
    _setSassCompilePaths = MONSTER.sassCompilePath
    _i = -1
    _num = _setSassCompilePaths.length
    _sassObj = null
    while ++_i < _num
      #_sassObj = {}#ここいるかな
      _sassObj =
        expand: true
        cwd: '<%= path.src %>/<%= path.sass %>/' + _setSassCompilePaths[_i][0]
        src: ['*.scss']
        dest: '<%= path.bin %>/' + _setSassCompilePaths[_i][1]
        ext: '.css'
      _sassFilesArr[_i] = _sassObj
    return
    # --------------------------------------------------------------
  # releaseフォルダにファイルをコピーする
  # --------------------------------------------------------------
  if MONSTER.releaseFiles.length > 0
    _releaseFiles = MONSTER.releaseFiles
    _i = -1
    _num = _releaseFiles.length
    _copyObj = {}
    while ++_i < _num
      _copyObj =
        expand: true
        cwd: '<%= path.bin %>/' + _releaseFiles[_i]
        src: '**'
        dest: '<%= path.release %>/' + _releaseFiles[_i]
      _releaseFilesArr[_i] = _copyObj

  grunt.initConfig
    # --------------------------------------------------------------
    # config
    # --------------------------------------------------------------
    pkg: _pkg
    path: MONSTER.path
    files: MONSTER.files
    # --------------------------------------------------------------
    # watch [ https://github.com/gruntjs/grunt-contrib-watch ]
    # --------------------------------------------------------------
    watch:
      sass:
        files: ['<%= path.src %>/<%= path.sass %>/**']
        tasks: ['sass', 'csscomb', 'autoprefixer', 'notify:sass']
      ts:
        files: ['<%= path.src %>/<%= path.ts %>/**/*.ts']
        tasks: ['typescript', 'copy:setTypescript', 'clean:typescript', 'notify:typescript']
    # --------------------------------------------------------------
    # browserSync [ https://github.com/shakyshane/grunt-browser-sync ]
    # --------------------------------------------------------------
    browserSync:
        dev:
          bsFiles:
            src: ['<%= path.bin %>/<%= path.css %>/*.css'
                  '<%= path.bin %>/**/*.html'
              '<%= path.bin %>/<%= path.js %>/*.js'
            ]
          options:
            server:
              baseDir: _browserSyncPath
            watchTask: true
    # --------------------------------------------------------------
    # bowercopy [ https://github.com/timmywil/grunt-bowercopy ]
    # --------------------------------------------------------------
    bowercopy:
      ibs:
        options:
          destPrefix: '<%= path.bin %>/<%= path.js %>/libs/'
          clean: true
        files: MONSTER.bowerFiles
    # --------------------------------------------------------------
    # copy [ https://github.com/gruntjs/grunt-contrib-copy ]
    # --------------------------------------------------------------
    copy:
      releaseFiles:
        files: _releaseFilesArr
      releaseImg:
        expand: true
        cwd: '<%= path.bin %>/<%= path.images %>/'
        src: '**'
        dest: '<%= path.release %>/<%= path.images %>/'
        filter: 'isFile'
      releaseLibsJs:
        expand: true
        cwd: '<%= path.bin %>/<%= path.js %>/libs'
        src: '**'
        dest: '<%= path.release %>/<%= path.js %>/libs'
        filter: 'isFile'
      setTypescript:
        expand: true
        cwd: '<%= path.bin %>/<%= path.js %>/<%= path.src %>/<%= path.ts %>'
        src: '**'
        dest: '<%= path.bin %>/<%= path.js %>'
        filter: 'isFile'
    # --------------------------------------------------------------
    # notify [ https://github.com/dylang/grunt-notify ]
    # --------------------------------------------------------------
    notify:
      sass:
        options:
          title: 'Sass'
          message: MONSTER.notifyMsg
      typescript:
        options:
          title: 'Typescript'
          message: 'Typescript Compile!!'
    # --------------------------------------------------------------
    #  clean [ https://github.com/gruntjs/grunt-contrib-clean ]
    # --------------------------------------------------------------
    clean:
      release:
        src: ['<%= path.release %>']
      releaseFiles:
        src: MONSTER.deleteFiles
      typescript:
        src: ['<%= path.bin %>/<%= path.js %>/<%= path.src %>']
    # --------------------------------------------------------------
    # concat [ https://github.com/gruntjs/grunt-contrib-concat ]
    # --------------------------------------------------------------
    concat:
      lib:
        src: MONSTER.libs
        dest: '<%= path.bin %>/<%= path.js %>/libs.js'
    
    # --------------------------------------------------------------
    # sass [ https://github.com/gruntjs/grunt-contrib-sass ]
    # --------------------------------------------------------------
    sass:
      options: MONSTER.sassOptions
      dist:
        files: _sassFilesArr
    # --------------------------------------------------------------
    # csscomb [ https://github.com/csscomb/grunt-csscomb ]
    # --------------------------------------------------------------
    csscomb:
      all:
        config: 'csscomb.json'
        expand: true
        cwd: '<%= path.bin %>/<%= path.css %>/'
        src: ['*.css']
        dest: '<%= path.bin %>/<%= path.css %>/'
    # --------------------------------------------------------------
    # autoprefixer [ https://github.com/nDmitry/grunt-autoprefixer ]
    # --------------------------------------------------------------
    autoprefixer:
      options:
        browsers: ['last 2 version', 'ie 8']
      main:
        expand: true
        flatten: true
        cwd: '<%= path.bin %>/<%= path.css %>/'
        src: ['*.css']
        dest: '<%= path.bin %>/<%= path.css %>/'
    # --------------------------------------------------------------
    # cmq [ https://github.com/buildingblocks/grunt-combine-media-queries ]
    # --------------------------------------------------------------
    cmq:
      options:
        log: false
      dev:
        config: 'csscomb.json'
        expand: true
        cwd: '<%= path.bin %>/<%= path.css %>/'
        src: ['*.css']
        dest: '<%= path.bin %>/<%= path.css %>/'
    # --------------------------------------------------------------
    # uncss [ https://github.com/asciidisco/grunt-requirejs ]
    # --------------------------------------------------------------
    uncss:
      dist:
        files:
          '<%= path.bin %>/<%= path.css %>/style.css': ['<%= path.bin %>/*.html']
    # --------------------------------------------------------------
    # sprite [ https://github.com/Ensighten/grunt-spritesmith ]
    # --------------------------------------------------------------
    sprite: {}

    # --------------------------------------------------------------
    # sassdoc [ https://github.com/SassDoc/grunt-sassdoc ]
    # --------------------------------------------------------------
    sassdoc: {}
    # --------------------------------------------------------------
    # validation [ https://github.com/praveenvijayan/grunt-html-validation ]
    # --------------------------------------------------------------
    validation:
      options:
        reset: grunt.option('reset') or false
        stoponerror: false
        relaxerror: ['Bad value X-UA-Compatible for attribute http-equiv on element meta.']
      files:
        src: ['<%= path.bin %>/**/*.html']
    # --------------------------------------------------------------
    # csslint [ https://github.com/gruntjs/grunt-contrib-csslint ]
    # --------------------------------------------------------------
    csslint:
      options:
        csslintrc: '.csslintrc'
      main:
        src: ['<%= path.bin %>/<%= path.css %>/*.css']
    # --------------------------------------------------------------
    # jshint [ https://github.com/gruntjs/grunt-contrib-jshint ]
    # --------------------------------------------------------------
    jshint:
      options:
        jshintrc: '.jshintrc'
        report: require('jshint-stylish')
      all:
        src: ['<%= path.bin %>/<%= path.js %>/*.js']
    # --------------------------------------------------------------
    # htmlmin [ https://github.com/gruntjs/grunt-contrib-htmlmin ]
    # --------------------------------------------------------------
    htmlmin:
      all:
        options:
          removeComments: true
          removeCommentsFromCDATA: true
          removeCDATASectionsFromCDATA: true
          collapseWhitespace: true
          removeRedundantAttributes: true
          removeOptionalTags: false
        expand: true
        cwd: '<%= path.bin %>/'
        src: ['<%= files.html %>']
        dest: '<%= path.release %>/'
    # --------------------------------------------------------------
    # csso [ https://github.com/t32k/grunt-csso ]
    # --------------------------------------------------------------
    csso:
      all:
        options:
          banner: MONSTER.banner,
          restructure: false
          report: 'min'
        expand: true
        cwd: '<%= path.bin %>/<%= path.css %>/'
        src: MONSTER.cssMinFiles
        dest: '<%= path.release %>/<%= path.css %>/'
        ext: '.min.css'
    # --------------------------------------------------------------
    # uglify [ https://github.com/gruntjs/grunt-contrib-uglify ]
    # --------------------------------------------------------------
    uglify:
      build:
        options:
          banner: MONSTER.banner
          sourceMap: MONSTER.isDebug
        src: '<%= path.bin %>/<%= path.js %>/main.js'
        dest: '<%= path.bin %>/<%= path.js %>/' + MONSTER.projectname + '.js'
    # --------------------------------------------------------------
    # imageoptim [ https://github.com/JamieMason/grunt-imageoptim ]
    # --------------------------------------------------------------
    imageoptim:
      src: ['bin']
      options:
        jpegMini: MONSTER.hasJpegMini
        imageAlpha: MONSTER.hasImageAlpha
        quitAfter: true
    # --------------------------------------------------------------
    # svgmin [ https://github.com/sindresorhus/grunt-svgmin ]
    # --------------------------------------------------------------
    svgmin:
      options:
        plugins: [
          {
            removeViewBox: false
          }
          {
            removeUselessStrokeAndFill: false
          }
        ]

      dist:
        files: [
          expand: true
          cwd: '<%= path.bin %>/img'
          src: ['**/*.svg']
          dest: '<%= path.release %>/img'
        ]
    # --------------------------------------------------------------
    # typescript [ https://github.com/k-maru/grunt-typescript ]
    # --------------------------------------------------------------
    typescript:
      main:
        options:
          module: 'amd' #or commonjs
          target: 'es5'
          base_path: '<%= path.src %>/<%= path.ts %>'
          watch: false,
          noLib: false
          ignoreError: true

        src: ['<%= path.src %>/<%= path.ts %>/**/*.ts']
        dest: '<%= path.bin %>/<%= path.js %>/'
    # --------------------------------------------------------------
    # requirejs [ https://github.com/asciidisco/grunt-requirejs ]
    # --------------------------------------------------------------
    requirejs:
      main:
        options:
          name: 'main'
          baseUrl: '<%= path.bin %>/<%= path.js %>/'
          mainConfigFile: '<%= path.bin %>/<%= path.js %>/main.js'
          out: '<%= path.release %>/<%= path.js %>/main.min.js'
          useStrict: true
          preserveLicenseComments: false
    # --------------------------------------------------------------
    # shared_config [ https://github.com/MathiasPaumgarten/grunt-shared-config ]
    # --------------------------------------------------------------
    shared_config:
      configAnimate:
        options:
          name: MONSTER.sharedConfig.name
          cssFormat: MONSTER.sharedConfig.cssFormat
          jsFormat: MONSTER.sharedConfig.jsFormat
          amd: false
        src: '<%= path.src %>/config/' + MONSTER.sharedConfig.name + '.json'
        dest: [
          '<%= path.src %>/<%= path.sass %>/helpers/_' + MONSTER.sharedConfig.name + '.scss'
          '<%= path.bin %>/<%= path.js %>/' + MONSTER.sharedConfig.name + '.js'
        ]
    # --------------------------------------------------------------
    # parallelize [ https://github.com/teppeis/grunt-parallelize ]
    # --------------------------------------------------------------
    parallelize: {}

  


  # --------------------------------------------------------------
  # JSファイルを圧縮する
  # --------------------------------------------------------------
  if MONSTER.jsMinFiles.length > 0
    _jsMinFiles = MONSTER.jsMinFiles
    _i = -1
    _num = _jsMinFiles.length
    _jsFile = ''
    while ++_i < _num
      _jsFile = _jsMinFiles[_i] + '.js'
      grunt.config.data.uglify[_jsMinFiles[_i]] =
        options:
          banner: MONSTER.banner
          mangle: true
        expand: true
        cwd: '<%= path.bin %>/<%= path.js %>'
        src: _jsMinFiles[_i]
        dest: '<%= path.release %>/<%= path.js %>/'
  #--------------------------------------------------------------
  # sprite画像を生成する
  #--------------------------------------------------------------
  if MONSTER.sprite.length > 0
    _i = -1
    _task = MONSTER.sprite
    _num = _task.length
    while ++_i < _num
      _task[_i][1].cssOpts =
        functions: false
      grunt.config.data.sprite[_task[_i][0]] = _task[_i][1]
  # --------------------------------------------------------------
  # pakage.jsonに記載されているパッケージを自動読み込み
  # --------------------------------------------------------------
  for taskName of _pkg.devDependencies
    grunt.loadNpmTasks taskName  if taskName.substring(0, 6) is 'grunt-'
  # --------------------------------------------------------------
  # 処理でエラーが出てもgruntを続ける
  # --------------------------------------------------------------
  grunt.registerTask 'eatwarnings', ->
    grunt.warn = grunt.fail.warn = (warning) ->
      grunt.log.error warning
      return
    return

  # --------------------------------------------------------------
  # Sassをコンパイルする
  # --------------------------------------------------------------
  grunt.registerTask 'default', [
    'browserSync'
    'watch:sass'
  ]

  # --------------------------------------------------------------
  # TSファイルをコンパイル
  # --------------------------------------------------------------
  grunt.registerTask 'ts', ['typescript']


  # --------------------------------------------------------------
  # 画像を圧縮
  # --------------------------------------------------------------
  grunt.registerTask 'imgmin', ['imageoptim']

  # --------------------------------------------------------------
  # libsのライブラリを一つのJSにconcat
  # --------------------------------------------------------------
  grunt.registerTask 'lib', ['concat:lib']

  # --------------------------------------------------------------
  # 納品ファイルを生成
  # --------------------------------------------------------------
  grunt.registerTask 'release', [
    'clean:release'
    'copy:releaseFiles'
    'copy:releaseLibsJs'
    'copy:releaseImg'
    'htmlmin'
    'csso'
    'doneRelease'
  ]
  grunt.registerTask 'doneRelease', 'bind javascript files.', ->
    grunt.task.run 'requirejs'  if MONSTER.hasRequireJs
    grunt.task.run 'uglify', 'clean:releaseFiles'
    return
  return