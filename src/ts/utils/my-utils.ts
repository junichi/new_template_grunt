/// <reference path="../libs/jquery/jquery.d.ts" />
/// <reference path="../libs/lodash/lodash.d.ts" />

import _ = require('lodash');

module Utils {
    var
        _doc = document,
        _window = window,
        _ua:string = navigator.userAgent,
        _app:string = navigator.appVersion.toLowerCase(),
        _this = Utils;

    export var isJquery = (obj):boolean => {
        if (obj instanceof jQuery) {
            return true;
        } else {
            return false;
        }
    }

    export var getElement = (id:string) => {
        return <HTMLElement>_doc.getElementById(id);
    }

    export var encapsulate = (context:string) => {
        return jQuery(context);
    }

    export var innerWidth = ():number => {
        return  _window.innerWidth | 0;
    }

    export var innerHeight = ():number => {
        return  _window.innerHeight | 0;
    }


    export var outerWidth = (el):number => {
        return  el.offsetWidth | 0;

    }

    export var outerWidthMargin = (el):number => {
        var _width = el.offsetWidth,
            _style = getComputedStyle(el);

        _width += parseInt(_style.marginTop) + parseInt(_style.marginBottom);
        return _width;
    }

    export var outerHeight = (el):number => {
        return  el.offsetHeight | 0;
    }

    export var outerHeightMargin = (el):number => {
        var _height = el.offsetHeight,
            _style = getComputedStyle(el);

        _height += parseInt(_style.marginTop) + parseInt(_style.marginBottom);
        return _height;
    }

    export var position = (el) => {
        return {left: el.offsetLeft, top: el.offsetTop};
    }

    export var getText = (el):string => {
        return el.textContent;
    }

    export var setText = (el, str:string) => {
        el.textContent = str;
    }

    export var find = (el, selector:string) => {
        return <HTMLElement>el.querySelectorAll(selector);
    }

    export var findClass = (el, selector:string) => {
        return <HTMLElement>el.getElementsByClassName(selector);
    }

    export var after = (el, htmlString:string) => {
        return <HTMLElement>el.insertAdjacentHTML('afterend', htmlString);
    }

    export var before = (el, htmlString:string) => {
        return <HTMLElement>el.insertAdjacentHTML('beforebegin', htmlString);
    }

    export var append = (el, parent) => {
        parent.appendChild(el);
    }

    export var prepend = (el, parent) => {
        parent.insertBefore(el, parent.firstChild);
    }

    export var remove = (el) => {
        el.parentNode.removeChild(el);
    };

    export var addClass = (el, className:string[]) => {
        var
            _i:number = -1,
            _length:number = className.length;

        if (!_.isArray(className)) {
            throw new TypeError('文字列の配列を指定');
        }

        for (; ++_i < _length;) {
            if (el.classList) {
                el.classList.add(className[_i]);
            } else {
                el.className += ' ' + className[_i];
            }
        }
    }

    export var removeClass = (el, className:string) => {
        if (!_.isString(className)) {
            throw new TypeError('String型を指定');
        }
        if (el.classList) {
            el.classList.remove(className);
        } else {
            el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
    }

    export var hasClass = (el, className:string):boolean => {
        if (!_.isString(className)) {
            throw new TypeError('String型を指定');
        }
        if (el.classList) {
            return el.classList.contains(className);
        } else {
            return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
        }
    }

    export var toggleClass = (el, className:string) => {
        if (el.classList) {
            el.classList.toggle(className);
        } else {
            var
                classes:string[] = el.className.split(' '),
                existingIndex:number = classes.indexOf(className);

            if (existingIndex >= 0) {
                classes.splice(existingIndex, 1);
            } else {
                classes.push(className);
            }

            el.className = classes.join(' ');
        }
    }

    export var getAtter = (el, type:string) => {
        return el.getAttribute(type);
    }

    export var setAttr = (el, type:string, val) => {
        el.setAttribute(type, val);
    }

    export var documentReady = (callback) => {
        _doc.addEventListener('DOMContentLoaded', callback, false);
    }

    export var isIE = ():boolean => {
        return _app.indexOf('msie ') > -1 ? true : false;
    }

    export var isIE6 = ():boolean => {
        return _this.isIE() && _app.indexOf('msie 6.0') > -1 ? true : false;
    }

    export var _isIE7 = ():boolean => {
        return _this.isIE() && _app.indexOf('msie 7.0') > -1 ? true : false;
    }

    export var _isIE8 = ():boolean => {
        return _this.isIE() && _app.indexOf('msie 8.0') > -1 ? true : false;
    }

    export var _isIE9 = ():boolean => {
        return _this.isIE() && _app.indexOf('msie 9.') > -1 ? true : false;
    }

    export var _isIE10 = ():boolean => {
        return _this.isIE() && _app.indexOf('msie 10.0') > -1 ? true : false;
    }

    export var isSafari = ():boolean => {
        return _ua.indexOf('safari') > -1 ? true : false;
    }

    export var isFirefox = ():boolean => {
        return _ua.indexOf('Firefox') > -1 ? true : false;
    }

    export var isChrome = ():boolean => {
        return _app.indexOf('chrome') > -1 ? true : false;
    }

    export var isiPhone = ():boolean => {
        return _ua.indexOf('iPhone') > -1 ? true : false;
    }

    export var isiPad = ():boolean => {
        return _ua.indexOf('iPad') > -1 ? true : false;
    }

    export var isIOS = ():boolean => {
        return _this.isiPhone() || _this.isiPad() ? true : false;
    }

    export var isAndroid = ():boolean => {
        return _ua.indexOf('Android') > -1 ? true : false;
    }

    export var isAndroidMobile = ():boolean => {
        return _this.isAndroid() && _ua.indexOf('Mobile') > -1 ? true : false
    }

    export var isAndroid2 = ():boolean => {
        return _ua.indexOf('Android 2') !== -1 ? true : false;
    }

    export var isAndroidTablet = ():boolean => {
        return (_this.isAndroid() && _ua.indexOf('Mobile') == -1) || _ua.indexOf('A1_07') > 0 || _ua.indexOf('SC-01C') > 0 ? true : false;
    }

    export var isAndroidChrome = ():boolean => {
        return _this.isAndroid() && _this.isChrome() ? true : false;
    }

    export var isAndroidBrowser = ():boolean => {
        return _this.isAndroid() && !_this.isChrome() ? true : false;
    }

    export var isPC = ():boolean => {
        return !_this.isiPhone() && !_this.isiPad() && !_this.isAndroid() ? true : false;
    }

    export var isSP = ():boolean => {
        return _this.isiPhone() || isAndroid() ? true : false;
    }

    export var getJson = (url:string) => {
        var _request = new XMLHttpRequest(),
            _data;
        _request.open('GET', url, true);

        _request.onload = ():any => {
            if (_request.status >= 200 && _request.status < 400) {
                _data = JSON.parse(_request.responseText);
                return _data;
            } else {

            }
        };

        _request.onerror = function () {
        };

        _request.send();
    }

    export var request = (url, callback:Function) => {
        var _request = new XMLHttpRequest(),
            _resp;
        _request.open('GET', 'url', true);

        _request.onload = function () {
            if (_request.status >= 200 && _request.status < 400) {
                // Success!
                _resp = _request.responseText;

                callback();
            } else {
                // We reached our target server, but it returned an error

            }
        };

        _request.onerror = function () {
            // There was a connection error of some sort
        };

        _request.send();
    }

    export var post = (url:string, data) => {
        var request = new XMLHttpRequest();
        request.open('POST', url, true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.send(data);
    }

    export var hide = (el) => {
        el.style.display = 'none';
    }

    export var show = (el) => {
        el.style.display = 'block';
    }

    export var opacity = (el, propaty:string) => {
        el.style.opacity = propaty;
    }

    export var width = (el, propaty:any) => {
        if (_.isString(propaty)) {
            el.style.width = propaty
        } else {
            el.style.width = propaty + 'px';
        }
    }

    export var height = (el, propaty:any) => {
        if (_.isString(propaty)) {
            el.style.height = propaty
        } else {
            el.style.height = propaty + 'px';
        }
    }

    export var postion = (el, propaty:number, flag:string = 'top') => {
        switch (flag) {
            case 'top':
                el.style.top = propaty + 'px';
                break;
            case 'right':
                el.style.right = propaty + 'px';
                break;
            case 'bottom':
                el.style.bottom = propaty + 'px';
                break;
            case 'left':
                el.style.left = propaty + 'px';
                break;
            default:
                break;
        }
    }

    export var margin = (el, propaty:number, flag:string = 'top') => {
        switch (flag) {
            case 'top':
                el.style.marginTop = propaty + 'px';
                break;
            case 'right':
                el.style.marginRight = propaty + 'px';
                break;
            case 'bottom':
                el.style.marginBottom = propaty + 'px';
                break;
            case 'left':
                el.style.marginLeft = propaty + 'px';
                break;
            default:
                break;
        }
    }
}


export = Utils;