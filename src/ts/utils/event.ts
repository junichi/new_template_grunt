var Event = {
    RESIZE: 'resize',
    ORIENTATION: 'orientationchange',
    LOAD: 'load',
    SCROLL: 'scroll',
    SELECT: 'select',
    SUBMIT: 'submit',
    HASH_CHANGE: 'hashchange',
    BLUR: 'blur',
    CHANGE: 'change',
    ABORT: 'unload',
    BEFORE_UNLOAD: 'beforeunload',
    ERROR: 'error',
    CONTEXT_MENU: 'contextmenu',
    COPY: 'copy',
    PASTE: 'paste',
    READY_STATE_CHANGE: 'readystatechange',
    RESET: 'reset',
    LOAD_END: 'loadend',
    POP_STATE: 'popstate'
};

export = Event;

