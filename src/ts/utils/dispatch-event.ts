module DispatchEvent {
    'use strict';
    export interface IEventDispatcher {
        hasEventListener(type:string, listener:Function):boolean;
        addEventListener(type:string, listener:Function):void;
        removeEventListener(type:string, listener:Function):void;
        dispatchEvent(event:DispatchEvent.CreateEvent):void;
    }

    export interface IEventListener {
        type:string;
        listener:Function;
    }

    export class CreateEvent {
        private type:string;
        private target:any;

        constructor(type:string, targetObj?:any) {
            this.type = type;
            this.target = targetObj;

        }

        public getTarget():any {
            return this.target;
        }

        public getType():string {
            return this.type;
        }
    }

    export class EventDispatcher implements IEventDispatcher {
        public listeners:IEventListener[] = [];

        public hasEventListener(type:string, listener:Function):boolean {
            var
                _exists:boolean = false,
                _i:number = -1,
                _length:number = this.listeners.length;

            for (; ++_i < _length;) {
                if (this.listeners[_i].type === type && this.listeners[_i].listener === listener) {
                    _exists = true;
                    return _exists;
                }
            }
            return _exists;
        }

        public addEventListener(type:string, listener:Function):void {
            if (this.hasEventListener(type, listener)) {
                return;
            }

            this.listeners.push({type: type, listener: listener});
        }

        public removeEventListener(type:string, listener:Function):void {
            var
                _i:number = -1,
                _length:number = this.listeners.length;

            for (; ++_i < _length;) {
                if (this.listeners[_i].type === type && this.listeners[_i].listener === listener) {
                    this.listeners.splice(_i, 1);
                }
            }
        }

        public dispatchEvent(event:DispatchEvent.CreateEvent):void {
            var
                _i:number = -1,
                _length:number = this.listeners.length;

            for (; ++_i < _length;) {
                if (this.listeners[_i].type === event.getType()) {
                    this.listeners[_i].listener.call(this, event);
                }
            }
        }

        public clearEvent():void {
            this.listeners = [];
        }
    }
}

export = DispatchEvent;

// Sample
//class Test extends DispatchEvent.EventDispatcher {
//    constructor() {
//        super();
//        var
//            _test:Function;
//
//        _test = function () {
//            alert('ok');
//        }
//        this.addEventListener('TESTEVENT', _test);
//
//        document.onclick = ()=> {
//            this.dispatchEvent(new DispatchEvent.CreateEvent('TESTEVENT', _test));
//            this.removeEventListener('TESTEVENT', _test);
//        };
//    }
//}
//
//var _test = new Test();