var KeyboardEvent = {
    KEY_DOWN: 'keydown',
    KEY_UP: 'keyup',
    KEY_PRESS: 'keypress'
};

export = KeyboardEvent;

