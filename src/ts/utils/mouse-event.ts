var MouseEvent = {
    CLICK: 'click',
    MOUSE_DOWN: 'mousedown',
    MOUSE_MOVE: 'mousemove',
    MOUSE_UP: 'mouseup',
    RIGHT_CLICK: 'rightclick',
    MOUSE_OVER: 'mouseover',
    MOUSE_OUT: 'mouseout',
    DOUBLE_CLICK: 'dblclick',
    FOCUS: 'focus',
    MOUSE_ENTER: 'mouseenter',
    MOUSE_LEAVE: 'mouseleave',
    ROLL_OVER: 'mouseenter',
    ROLL_OUT: 'mouseleave',
    DRAG_END: 'dragend',
    DRAG_ENTER: 'dragenter',
    DRAG_LEAVE: 'dragleave',
    DRAG_OVER: 'dragover',
    DRAG_START: 'dragstart',
    DROP: 'drop',
    MOUSE_WHEEL: 'mousewheel'
};

export = MouseEvent;

