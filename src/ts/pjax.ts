/// <reference path="libs/greensock/greensock.d.ts" />

import Utils = require('utils/event');

interface IStatus {
    START: number;
    SEND: number;
    BEFORESEND: number;
    COMPLETE: number;
    END: number;
}


class Pjax {
    public pjaxObj:{
        container:string;
        links:string;
        timeout:number;
    };
    private onOutComplete:Function;
    private onInComplete:Function;
    private el;
    private xhr;
    private win;
    private status:number = 0;
    private isPageOutComplete:boolean = false;
    private STATUS_CODE:IStatus = {
        START: 1,
        BEFORESEND: 2,
        SEND: 4,
        COMPLETE: 8,
        END: 15
    };


    public init(obj, onOutComplete, onInComplete) {
        this.pjaxObj = obj;
        this.el = document.getElementById(this.pjaxObj.container);
        this.xhr = new XMLHttpRequest();
        this.win = window;
        this.setEvent();

        this.onOutComplete = onOutComplete;
        this.onInComplete = onInComplete;

        var _url = location.href.replace(/index\..+/, '');
        history.replaceState({url: _url}, null, _url);

    }

    private setEvent() {
        var _doc = document,
            _timer, _onReadyStateChange:EventListener, _onPopstate:EventListener,
            _callback:Function, _onError:Function, _setAnimation;

        _setAnimation = () => {
            if (this.isPageOutComplete) {
                this.isPageOutComplete = false;
                clearTimeout(_timer);

                _callback = () => {
                    this.status |= this.STATUS_CODE.END;
                    if (this.status && this.STATUS_CODE.END === this.STATUS_CODE.END) {
                        this.status = 0;
                        this.onInComplete();
                        //console.log('05.END//////////////////////////////////////////////');
                    }
                };

                var _response = this.xhr.response,
                    _responseElement = _response.getElementById(this.pjaxObj.container);

                _doc.title = _response.title;
                this.el.innerHTML = _responseElement.innerHTML;
                this.status |= this.STATUS_CODE.SEND;
                if (this.status && this.STATUS_CODE.SEND === this.STATUS_CODE.SEND) {
                    //console.log('03.SEND');
                }
                this.status |= this.STATUS_CODE.COMPLETE;
                if (this.status && this.STATUS_CODE.COMPLETE === this.STATUS_CODE.COMPLETE) {
                    //console.log('04.COMPLETE');
                    this.pageIn(_callback);
                    _response = _responseElement = null;
                    _response = _responseElement = void 0;
                }
            } else {
                _timer = setTimeout(_setAnimation, 300);
            }
        };

        _onError = () => {
            console.log('読み込みに失敗しました');
        };

        _onReadyStateChange = () => {
            if (this.xhr.readyState === 4 && this.xhr.status == 200) {
                _setAnimation();
            }
        };

        _onPopstate = (event:PopStateEvent) => {
            var _callback:Function;
            event.preventDefault();
            _callback = () => {
                this.request(event.state.url);
                this.onOutComplete();
            };
            if (event.state) {
                this.pageOut(_callback);
            }
        };

        //this.xhr.addEventListener('loadend', _onLoadend);
        this.xhr.addEventListener('readystatechange', _onReadyStateChange);
        this.win.addEventListener('popstate', _onPopstate);
    }

    private setAnimation() {
        var _doc = document,
            _response, _responseElement,
            _startAnimation:Function,
            _timer, _callback:Function;

        _startAnimation = ():void => {
            if (this.isPageOutComplete) {
                this.isPageOutComplete = false;
                clearTimeout(_timer);

                _callback = () => {
                    this.status |= this.STATUS_CODE.END;
                    if (this.status && this.STATUS_CODE.END === this.STATUS_CODE.END) {
                        this.status = 0;
                        this.onInComplete();
                        console.log('05.END//////////////////////////////////////////////');
                    }
                };

                _response = this.xhr.response;
                _responseElement = _response.getElementById(this.pjaxObj.container);

                _doc.title = _response.title;

                this.el.innerHTML = _responseElement.innerHTML;
                this.status |= this.STATUS_CODE.SEND;

                if (this.status && this.STATUS_CODE.SEND === this.STATUS_CODE.SEND) {
                    //console.log('03.SEND');
                }
                this.status |= this.STATUS_CODE.COMPLETE;
                if (this.status && this.STATUS_CODE.COMPLETE === this.STATUS_CODE.COMPLETE) {
                    //console.log('04.COMPLETE');

                    _doc = _response = _responseElement = _timer = _callback = null;
                    _doc = _response = _responseElement = _timer = _callback = void 0;
                }
                this.pageIn(_callback);
            } else {
                _timer = setTimeout(_startAnimation, 300);
            }
        };

        _startAnimation();
    }


    public onPjax(url:string) {
        var _callback:Function;

        this.status |= this.STATUS_CODE.START;
        if (this.status && this.STATUS_CODE.START === this.STATUS_CODE.START) {
            //console.log('01.START//////////////////////////////////////////////');
        }
        var _url:string = url.replace(/index\..+/, '');

        _callback = () => {
            if (_url !== location.href) {
                history.pushState({url: _url}, null, _url);
            } else {
                //console.log('同じURLだよ');
            }
            this.request(_url);
            this.onOutComplete();

            _callback = null;
            _callback = void 0;
        };

        this.pageOut(_callback);
    }


    private request(url:string) {
        this.status |= this.STATUS_CODE.BEFORESEND;
        if (this.status && this.STATUS_CODE.BEFORESEND === this.STATUS_CODE.BEFORESEND) {
            //console.log('02.BEFORESEND');
        }

        this.xhr.abort();
        this.xhr.open('GET', url);
        this.xhr.setRequestHeader('X-AJAX-REQUEST', 'XMLHttpRequest');
        this.xhr.responseType = 'document';
        this.xhr.overrideMimeType('text/html; charset=utf-8');
        this.xhr.timeout = this.pjaxObj.timeout;
        this.xhr.send(null);
    }

    private pageIn(callback?:Function) {
        var _tl:TimelineLite = new TimelineLite(),
            _onTlComplete;

        _onTlComplete = () => {
            _tl.kill();
            _tl = _onTlComplete = null;
            _tl = _onTlComplete = void 0;
        };

        if (typeof callback === 'function') {
            callback();
        }

        _tl.fromTo(this.el, 0.5, {
            opacity: 0.01
        }, {
            opacity: 1,
            onComplete: _onTlComplete
        });
    }

    private pageOut(callback?:Function) {
        var _tl:TimelineLite = new TimelineLite(),
            _onTlComplete;

        _onTlComplete = () => {
            this.isPageOutComplete = true;
            _tl.kill();
            _tl = _onTlComplete = null;
            _tl = _onTlComplete = void 0;
        };

        if (typeof callback === 'function') {
            callback();
        }

        _tl.fromTo(this.el, 0.5, {
            opacity: 1
        }, {
            opacity: 0.01,
            onComplete: _onTlComplete
        });
    }

    public checkState() {
        return this.status;
    }
}

export = Pjax;

