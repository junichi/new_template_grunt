/// <reference path="pjax" />
/// <reference path="common" />
/// <reference path="utils/dispatch-event" />

import Common = require('common');
import Pjax = require('pjax');
import DispatchEvent = require('utils/dispatch-event');

class Facade extends DispatchEvent.EventDispatcher {
    private common:Common;
    private pjax:Pjax;

    public test() {
        var
            _obj = {
                url: 'test',
                container: 'data-page',
                timeout: 10000
            },
            _complete = () => {

                console.log('callback!!!!!!!!!!!!!');
                this.dispatchEvent(new DispatchEvent.CreateEvent('PJAX:END'));
            },
            _test:Function = () => {
                console.log('今のページは' + document.title);
            };

        this.common = new Common();
        this.pjax = new Pjax();
        this.pjax.init(_obj, _complete);
        this.addEventListener('PJAX:END', _test);

        console.log(this);
    }
}

export = Facade;



