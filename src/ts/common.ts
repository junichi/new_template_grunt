/// <reference path="libs/greensock/greensock.d.ts" />

module Common {
    'use strict';

    export interface ISmoothScrollProp {
        duration:number;
        easing;
    }

    export class SmoothScroll {
        private prop:ISmoothScrollProp = {
            duration: 0.6,
            easing: Power3.easeOut
        };

//        public set(eventObj, el:string, callback?:Function, prop? = this.prop):void {
//            var _doc = document,
//                _html = <HTMLElement>_doc.getElementsByTagName('body'),
//                _target = <HTMLElement>_doc.getElementById(el),
//                _targetPos:number = _target.offsetTop,
//                _tw:TimelineLite = new TimelineLite(),
//                _onComplete:Function, _onClick:EventListener;
//
//            this.prop = prop;
//
//            _onComplete = ():void => {
//                if (typeof callback === 'function') {
//                    callback();
//                }
//                eventObj.addEventListener('click', _onClick);
//            };
//
//            _onClick = (event) => {
//                eventObj.removeEventListener('click', _onClick);
//                _tw.to(_html, this.prop.duration, {
//                    scrollTop: _targetPos,
//                    ease: prop.easing
//                }).call(_onComplete);
//            };
//
//            eventObj.addEventListener('click', _onClick);
//        }
    }
}

export = Common;


