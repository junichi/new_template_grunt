module Observer {
    export class ObserverBase {
        public update() {

        }
    }

    export class ObserverList {
        public observerList = [];

        public add(obj) {
            return this.observerList.push(obj);
        }

        public empty() {
            this.observerList = [];
        }

        public count():number {
            return this.observerList.length;
        }

        public get(index) {
            if (index > -1 && index < this.observerList.length) {
                return this.observerList[index];
            }
        }

        public insert(obj, index) {
            var _pointer:number = -1;
            if (index === 0) {
                this.observerList.unshift(obj);
                _pointer = index;
            } else if (index === this.observerList.length) {
                this.observerList.push(obj);
                _pointer = index;
            }

            return _pointer;
        }

        public indexOf(obj, startIndex) {
            var _i = startIndex,
                _pointer = -1;

            while (_i < this.observerList.length) {
                if (this.observerList[_i] === obj) {
                    _pointer = _i;
                }
            }

            _i += 1;

            return _pointer;
        }

        public removeIndexAt(index) {
            if (index === 0) {
                this.observerList.shift();
            } else if (index === this.observerList.length - 1) {
                this.observerList.pop();
            }
        }
    }

    export class Subject {
        public observers = new Observer.ObserverList();

        public addObserver(observer) {
            this.observers.add(observer);
        }

        public removeObserver(observer) {
            this.observers.removeIndexAt(this.observers.indexOf(observer, 0));
        }

        public notify(context) {
            var _i:number = -1,
                _observerCount = this.observers.count();

            for (; ++_i < _observerCount;) {
                this.observers.get(_i).update(context);
            }

        }
    }
}