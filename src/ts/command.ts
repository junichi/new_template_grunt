/// <reference path="libs/lodash/lodash.d.ts" />
module Command {
    'use strict';

    export interface ICommandData {
        name:string;
        time:number;
        isComplete:boolean;
    }

    export interface ICommand {
        execute:Function;
        data:ICommandData;
    }

    export interface ICommonCommand {
        add(command:Command.BaseCommand):Command.CommonCommand;
        execute();
        wait(waitCompleteFunc:Function, waiteTime:number):void;
        reset():void;
        play():void;
        pause():void;
        setCompleteFunc(func:Function):void;
        commandComplete():void;

    }

    /**
     * Commandを生成するClass
     */
    export class BaseCommand implements ICommand {
        public execute:Function;
        public undo:Function;
        public data:ICommandData;

        /**
         * Commandをセットする
         * @param execute {Function}
         * @param data {Object}
         * @param undo {Function}
         */
            constructor(execute:Function, data:ICommandData, undo?:Function) {
            this.execute = execute;
            this.data = data;
            this.undo = undo;
        }
    }

    /**
     * ParallelCommand / SerialCommandのextend用のClas
     */
    export class CommonCommand implements ICommonCommand {
        public isPause:boolean = false;
        public isCompleteFunc:boolean = false;
        public commands:Command.BaseCommand[] = [];
        public waitTime:number;
        public waitCompleteFunc:Function;
        public completeFunc:Function;


        /**
         * 配列にCommandをpushする
         * @param command {Command.BaseCommand}
         * @returns {Command.CommonCommand} チェーンメソッドを使用するため自身を戻す
         */
        public add(command:Command.BaseCommand):Command.CommonCommand {
            this.commands.push(command);
            return this;
        }

        /**
         * 配列に格納されたコマンドを実行する
         * 子Classでオーバーライドする
         */
        public execute() {

        }

        /**
         * コマンドを待たせる
         * @param waitCompleteFunc
         * @param waiteTime
         */
        public wait(waitCompleteFunc:Function, waiteTime:number):void {
            var _waiteTime:number = waiteTime * 1000,
                _onComplete:Function;

            _onComplete = () => {
                this.waitCompleteFunc();
            };

            this.waitCompleteFunc = waitCompleteFunc;
            this.waitTime = waiteTime;
            setTimeout(_onComplete, _waiteTime);
        }

        /**
         * コマンドを実行する
         */
        public play():void {
            this.isPause = false;
            this.execute();
        }

        /**
         * コマンドを止める
         */
        public pause():void {
            this.isPause = true;
        }


        /**
         * Complete時に実行する関数を登録
         * @param func {Function}
         */
        public setCompleteFunc(func:Function):void {
            this.completeFunc = func;
            this.isCompleteFunc = true;
        }

        /**
         * コマンドが全て実行された時に行う処理
         */
        public commandComplete():void {
            if (this.isCompleteFunc) {
                this.completeFunc();
            }
            // setTimeout(this.reset, 100);
            this.reset();
        }

        /**
         * 値を初期化する
         */
        public reset():void {
            this.commands = [];
            this.isPause = false;
            this.isCompleteFunc = false;
            this.waitTime = 0;
        }

    }

    /**
     * 同時にコマンドを実行するClass
     */
    export class ParallelCommand extends Command.CommonCommand {
        constructor() {
            super();
        }

        public execute() {
            super.execute();
            var _i:number = -1,
                _length:number = this.commands.length,
                _timeArr:number[] = [],
                _counter:number = 0,
                _completeTime:number = 0,
                _setExecute:Function,
                _onComplete:Function;

            _onComplete = () => {
                this.commandComplete();
            };

            for (; ++_i < _length;) {
                _timeArr[_i] = this.commands[_i].data.time * 1000;
            }

            //_completeTime = _.max(_timeArr);
            _completeTime = _timeArr[0];

            _setExecute = () => {
                if (_length - 1 >= _counter) {
                    this.commands[_counter].execute();
                    this.commands[_counter].data.isComplete = true;
                    _counter += 1;
                    _setExecute();
                } else {
                    // TODO timeのMax値を取得
                    setTimeout(_onComplete, _timeArr[0]);
                }
            };

            _setExecute();
        }


    }

    /**
     * 順番にコマンドを実行するClass
     */
    export class SerialCommand extends Command.CommonCommand {
        private counter:number = 0;

        constructor() {
            super();
        }

        public execute() {
            super.execute();
            var _length:number = this.commands.length,
                _waiteTime:number = 0,
                _setExecute:Function;

            _setExecute = ():boolean => {
                if (_length > this.counter) {
                    if (this.isPause) {
                        return false;
                    }
                    this.commands[this.counter].execute();
                    this.commands[this.counter].data.isComplete = true;
                    _waiteTime = this.commands[this.counter].data.time * 1000;
                    setTimeout(_setExecute, _waiteTime);
                    this.counter += 1;
                } else {
                    this.commandComplete();
                    return true;
                }
            };
            setTimeout(_setExecute, 0.1);
        }

        public reset():void {
            super.execute();
            this.counter = 0;
        }
    }
}

//var _sample1, _sample2, _sample3, _sample3_2, _sample4,
//    _command1, _command2, _command3, _command3_2, _command3_2, _command4,
//    _prop = {
//        name: 'test',
//        time: 1,
//        isComplete: false
//    },
//    _serialCommand = new Command.SerialCommand(),
//    _parallelCommand = new Command.ParallelCommand();
//
//_sample1 = function () {
//    console.log('sample1');
//};
//_sample2 = function () {
//    console.log('sample2');
//};
//_sample3 = function () {
//    console.log('sample3');
//    _serialCommand.pause();
//    _serialCommand.wait(function(){
//        _serialCommand.play();
//    }, 2);
//};
//
//_sample3_2 = function() {
//    console.log('sample3');
//}
//
//_sample4 = function () {
//    console.log('sample4');
//};
//
//_command1 = new Command.BaseCommand(_sample1, _prop);
//_command2 = new Command.BaseCommand(_sample2, _prop);
//_command3 = new Command.BaseCommand(_sample3, _prop);
//_command3_2 = new Command.BaseCommand(_sample3_2, _prop);
//_command4 = new Command.BaseCommand(_sample4, _prop);
//
//_serialCommand
//    .add(_command1)
//    .add(_command2)
//    .add(_command3)
//    .add(_command4);
//
//_parallelCommand
//    .add(_command1)
//    .add(_command2)
//    .add(_command3_2)
//    .add(_command4);
//
//_serialCommand.setCompleteFunc(function () {
//    console.log('_serialCommand:complete', _command4.data);
//});
//
//_parallelCommand.setCompleteFunc(function () {
//    console.log('_parallelCommand:complete:', _command1.data);
//});
//
//
//_serialCommand.execute();
//_parallelCommand.execute(
// );


