/// <reference path="libs/requirejs/require.d.ts" />
/// <reference path="Facade.ts" />

require.config({
    baseUrl: "../js/",
    paths: {
        tweenMax: 'libs/TweenMax.min',
        lodash: 'libs/lodash.min',
        MyUtils: 'utils/my-utils',
        Facade: 'facade'
    },
    shim: {
        'MyUtils': {
            deps: ['lodash']
        }
    }
});

require([
    'tweenMax',
    'lodash',
    'MyUtils',
    'Facade'
], (tweenMax, lodash, MyUtils, Facade) => {
    'use strict';
    console.log('start');
    var _facade = new Facade();
    _facade.test();
});
