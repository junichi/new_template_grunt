/// <reference path="libs/jquery/jquery.d.ts" />
/// <reference path="libs/greensock/greensock.d.ts" />
/// <reference path="libs/lodash/lodash.d.ts" />
/// <reference path="worker.ts" />

declare var window:any;
import Utils = require('utils/my-utils');

class Loader {
    private isWorker:boolean = (!window.Worker) ? false : true;
    //private wokerPath:string = '/js/worker.js';
    private wokerPath:string = '/testup/2014/dns_zone/sp/www/js/worker.js';
    private worker:Worker;

    constructor() {
        if (this.isWorker) {
            this.worker = new Worker(this.wokerPath);
        }
    }

    public setLoader(loadList:string[], callback, completeType:number = loadList.length):void {
        var _isAndroidBrowser:boolean = Utils.isAndroidBrowser();
        (this.isWorker && !_isAndroidBrowser) ?
            this.setWorker(loadList, callback, completeType) :
            this.setAsync(loadList, callback);
    }

    private setWorker(loadList:string[], callback:Function, completeType:number = loadList.length):void {
        var _loadedArr:string[] = [],
            _onComplete:Function;

        _onComplete = () => {
            callback();
            _loadedArr = _onComplete = null;
            _loadedArr = _onComplete = void 0;

        };

        this.worker.postMessage(loadList);
        this.worker.onmessage = (event) => {
            if (event.data !== 'complete' || completeType > loadList.length) {
                _loadedArr.push(event.data);
            } else {
                setTimeout(_onComplete, 100);
            }

        }
    }

    private setAsync(loadList:string[], callback:Function) {
        var
            _isCatch = true,
            _count:number = 0,
            _newUrl:string = '',
            _path:string = '../',
            _results:string[] = [],
            _value:string = '',
            _xhr, _date,
            _returnUrl:Function, _loadComplete:Function,
            _load:Function;

        _returnUrl = (url) => {
            setTimeout(function () {
                _load(loadList[_count]);
            }, 10);
        };

        _loadComplete = (url) => {
            callback();
            _results = _xhr = _returnUrl = _loadComplete = _load = null;
            _results = _xhr = _returnUrl = _loadComplete = _load = void 0;
        };

        _load = (url) => {
            if (!_isCatch) {
                _date = new Date;
                _value = '?' + (((_date.getYear() + _date.getMilliseconds()) * Math.random()) | 0);
            }
            _newUrl = _path + url + _value;
            _count += 1;
            _xhr = new XMLHttpRequest();
            _xhr.open('GET', url, false);
            _xhr.send();
            _results.push(_xhr.responseText);
            _count < loadList.length ? _returnUrl(url) : _loadComplete(url);
        }

        _load(loadList[_count]);
    }
}


export = Loader;
