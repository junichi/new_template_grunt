declare function postMessage(message:any):void;

var onmessage = (event:MessageEvent):any => {
    'use strict';
    var
        _isCatch:boolean = true,
        _count:number = 0,
        _urls:string = event.data,
        _newUrl:string = '',
        _path:string = '../',
        _results:string[] = [],
        _value:string = '',
        _xhr, _date,

        _returnUrl = (url):void => {
            postMessage(url);
            setTimeout(function () {
                _load(_urls[_count]);
            }, 10);
        },
        _loadComplete = (url):void => {
            console.log('_loadComplete');
            postMessage(url);
            postMessage('complete');
        },
        _load = (url):void => {
            if (!_isCatch) {
                _date = new Date;
                _value = '?' + (((_date.getYear() + _date.getMilliseconds()) * Math.random()) | 0);
            }
            _newUrl = _path + url + _value;
            _count += 1;
            _xhr = new XMLHttpRequest();
            _xhr.open('GET', url, false);
            _xhr.send();
            // if (_xhr.status !== 200) { throw Error(_xhr.status + ' ' + _xhr.statusText + ': ' + _newUrl); }
            _results.push(_xhr.responseText);
            _count < _urls.length ? _returnUrl(url) : _loadComplete(url);
        };

    _load(_urls[_count]);
};
