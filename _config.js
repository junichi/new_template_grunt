/**
 * config.js
 *
 * version 5.0.0
 * update 11/28/14
 *
 * Copyright (c) 2014 Kentaro Otsuka, Junichi Honda All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * ・Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * ・Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * ・Neither the name of the <ORGANIZATION> nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

(function (module) {
    'use strict';
    var CONFIG = {
        // browserSyncのパスを設定
        // true : bin, false:release
        isDebug: true,
        path: {
            src: 'src',
            bin: 'bin',
            release: 'release',
            js: 'js',
            css: 'css',
            images: 'images',
            sass: 'sass',
            ts: 'ts'
        },
        files: {
            html: '**/*.html',
            css: '<%= path.css %>/**',
            js: '<%= path.js %>/**',
            images: '<%= dir.images %>/**'
        },
        sassOptions: {
            style: 'expanded',
            sourcemap: 'none'
        },
        // sassファイルへのパス, 出力されるcssファイルのパス
        sassCompilePath: [
            ['/', '<%= path.css %>/']
        ],
        sprite: [
            ['sprite_sample', {
                imgPath: '../images/sprite_sample.png',
                src: '<%= path.src %>/sprite/sprite_sample/*.png',
                destCSS: '<%= path.src %>/<%= path.sass %>/components/sprite/sprite_sample.scss',
                destImg: '<%= path.bin %>/<%= path.images %>/sprite_sample.png'
            }]
        ],
        cssMinFiles: [
            'style.css'
        ],
        jsMinFiles: [
            'command'
        ],
        notifyMsg: '★Sass Compile!!★',
        // 画像を圧縮するツールの設定
        hasJpegMini: true,
        hasImageAlpha: true,
        // releaseからコピーするファイルを選択
        releaseFiles: [
            'docs/'
        ],
        // releaseから削除するファイルを選択
        deleteFiles: [
            '<%= path.release %>/docs/'
        ],
        bowerFiles: {
            'jquery.min.js': 'jquery/dist/jquery.min.js',
            'require.js': 'requirejs/require.js',
            'lodash.min.js': 'lodash/dist/lodash.min.js',
            'TweenMax.min.js': 'gsap/src/minified/TweenMax.min.js'
        },
        hasRequireJs: false
    };

    module.exports = CONFIG;

}(module));

