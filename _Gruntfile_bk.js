/**
 * Gruntfile.js
 *
 * version 5.0.0
 * update 11/28/14
 *
 * Copyright (c) 2014 Kentaro Otsuka, Junichi Honda All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * ・Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * ・Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * ・Neither the name of the MONSTER-DIVE nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

'use strict';
var CONFIG = require('./config.js'),
    _sassFilesArr = [],
    _releaseFilesArr = [],
    _browserSyncPath,
    _setSassCompile;

CONFIG.isDebug ? _browserSyncPath = CONFIG.path.bin : _browserSyncPath = CONFIG.path.release;


//--------------------------------------------------------------
//  コンパイルするSassファイルをSassタスクに追加
//--------------------------------------------------------------
_setSassCompile = function () {
    var
        _length = CONFIG.sassCompilePath.length,
        _i = -1,
        _sassObj = {};

    for (_i, _length; ++_i < _length;) {
        _sassObj.expand = true;
        _sassObj.cwd = '<%= path.src %>/<%= path.sass %>/' + CONFIG.sassCompilePath[_i][0];
        _sassObj.src = ['*.scss'];
        _sassObj.dest = '<%= path.bin %>/' + CONFIG.sassCompilePath[_i][1];
        _sassObj.ext = '.css';
        _sassFilesArr[_i] = _sassObj;

        _sassObj = {};
    }
};

_setSassCompile();


//--------------------------------------------------------------
//  releaseフォルダにファイルをコピーする
//--------------------------------------------------------------
if (CONFIG.releaseFiles.length > 0) {
    (function () {
        var _i = -1,
            _length = CONFIG.releaseFiles.length,
            _copyObj = {};
        for (; ++_i < _length;) {
            _copyObj.expand = true;
            _copyObj.cwd = '<%= path.bin %>/' + CONFIG.releaseFiles[_i];
            _copyObj.src = '**';
            _copyObj.dest = '<%= path.release %>/' + CONFIG.releaseFiles[_i];
            _releaseFilesArr[_i] = _copyObj;
            _copyObj = {};
        }
    }());
}

module.exports = function (grunt) {
    var
        pkg = grunt.file.readJSON('package.json'),
        taskName;
    grunt.initConfig({
        pkg: pkg,
        path: CONFIG.path,
        files: CONFIG.files,
        //--------------------------------------------------------------
        //  watch [ https://github.com/gruntjs/grunt-contrib-watch ]
        //--------------------------------------------------------------
        watch: {
            sass: {
                files: ['<%= path.src %>/<%= path.sass %>/**'],
                tasks: ['sass', 'csscomb', 'autoprefixer', 'notify:sass'],
                options: {
                    livereload: true,
                    nospawn: true
                }
            }
        },
        //--------------------------------------------------------------
        //  browserSync [ https://github.com/shakyshane/grunt-browser-sync ]
        //--------------------------------------------------------------
        browserSync: {
            dev: {
                options: {
                    server: {
                        baseDir: 'bin'
                    },
                    watchTask: true
                }
            }
        },
        //--------------------------------------------------------------
        //  bowercopy [ https://github.com/timmywil/grunt-bowercopy ]
        //--------------------------------------------------------------
        bowercopy: {
            ibs: {
                options: {
                    destPrefix: '<%= path.bin %>/<%= path.js %>/libs/',
                    clean: true
                },
                files: CONFIG.bowerFiles
            }
        },
        //--------------------------------------------------------------
        //  copy [ https://github.com/gruntjs/grunt-contrib-copy ]
        //--------------------------------------------------------------
        copy: {
            releaseFiles: {
                files: _releaseFilesArr
            },
            releaseImg: {
                expand: true,
                cwd: '<%= path.bin %>/<%= path.images %>/',
                src: '**',
                dest: '<%= path.release %>/<%= path.images %>/',
                filter: 'isFile'
            }
        },
        //--------------------------------------------------------------
        //  notify [ https://github.com/dylang/grunt-notify ]
        //--------------------------------------------------------------
        notify: {
            sass: {
                options: {
                    title: 'Sass',
                    message: CONFIG.notifyMsg
                }
            }
        },
        //--------------------------------------------------------------
        //  clean [ https://github.com/gruntjs/grunt-contrib-clean ]
        //--------------------------------------------------------------
        clean: {
            release: {
                src: ['<%= path.release %>']
            },
            releaseFiles: {
                src: CONFIG.deleteFiles
            }
        },
        //--------------------------------------------------------------
        //  concat [ https://github.com/gruntjs/grunt-contrib-concat ]
        //--------------------------------------------------------------
        concat: {
        },
        //--------------------------------------------------------------
        //  sass [ https://github.com/gruntjs/grunt-contrib-sass ]
        //--------------------------------------------------------------
        sass: {
            options: CONFIG.sassOptions,
            dist: {
                files: _sassFilesArr
            }
        },
        //--------------------------------------------------------------
        //  csscomb [ https://github.com/csscomb/grunt-csscomb ]
        //--------------------------------------------------------------
        csscomb: {
            all: {
                config: 'csscomb.json',
                expand: true,
                cwd: '<%= path.bin %>/<%= path.css %>/',
                src: ['*.css'],
                dest: '<%= path.bin %>/<%= path.css %>/'
            }
        },
        //--------------------------------------------------------------
        //  autoprefixer [ https://github.com/nDmitry/grunt-autoprefixer ]
        //--------------------------------------------------------------
        autoprefixer: {
            options: {
                browsers: ['last 2 version', 'ie 8']
            },
            main: {
                expand: true,
                flatten: true,
                cwd: '<%= path.bin %>/<%= path.css %>/',
                src: ['*.css'],
                dest: '<%= path.bin %>/<%= path.css %>/'
            }
        },
        //--------------------------------------------------------------
        //  cmq [ https://github.com/buildingblocks/grunt-combine-media-queries ]
        //--------------------------------------------------------------
        cmq: {
            options: {
                log: false
            },
            dev: {
                config: 'csscomb.json',
                expand: true,
                cwd: '<%= path.bin %>/<%= path.css %>/',
                src: ['*.css'],
                dest: '<%= path.bin %>/<%= path.css %>/'
            }
        },
        //--------------------------------------------------------------
        //  uncss [ https://github.com/asciidisco/grunt-requirejs ]
        //--------------------------------------------------------------
        uncss: {
            dist: {
                files: {
                    '<%= path.bin %>/<%= path.css %>/style.css': ['<%= path.bin %>/*.html']
                }
            }
        },
        //--------------------------------------------------------------
        //  sprite [ https://github.com/Ensighten/grunt-spritesmith ]
        //--------------------------------------------------------------
        sprite: {
        },
        //--------------------------------------------------------------
        //  sassdoc [ https://github.com/SassDoc/grunt-sassdoc ]
        //--------------------------------------------------------------
        sassdoc: {
        },
        //--------------------------------------------------------------
        //  validation [ https://github.com/praveenvijayan/grunt-html-validation ]
        //--------------------------------------------------------------
        validation: {
            options: {
                reset: grunt.option('reset') || false,
                stoponerror: false,
                relaxerror: ['Bad value X-UA-Compatible for attribute http-equiv on element meta.'] //ignores these errors
            },
            files: {
                src: ['<%= path.bin %>/**/*.html']
            }
        },
        //--------------------------------------------------------------
        //  csslint [ https://github.com/gruntjs/grunt-contrib-csslint ]
        //--------------------------------------------------------------
        csslint: {
            options: {
                csslintrc: '.csslintrc'
            },
            main: {
                src: ['<%= path.bin %>/<%= path.css %>/*.css']
            }
        },
        //--------------------------------------------------------------
        //  jshint [ https://github.com/gruntjs/grunt-contrib-jshint ]
        //--------------------------------------------------------------
        jshint: {
            options: {
                csslintrc: '.csslintrc'
            },
            main: {
                src: ['<%= path.bin %>/<%= path.css %>/*.css']
            }
        },

        //--------------------------------------------------------------
        //  htmlmin [ https://github.com/gruntjs/grunt-contrib-htmlmin ]
        //--------------------------------------------------------------
        htmlmin: {
            all: {
                options: {
                    removeComments: true,
                    removeCommentsFromCDATA: true,
                    removeCDATASectionsFromCDATA: true,
                    collapseWhitespace: true,
                    removeRedundantAttributes: true,
                    removeOptionalTags: false
                },
                expand: true,
                cwd: '<%= path.bin %>/',
                src: ['<%= files.html %>'],
                dest: '<%= path.release %>/'
            }
        },
        //--------------------------------------------------------------
        //  csso [ https://github.com/t32k/grunt-csso ]
        //--------------------------------------------------------------
        csso: {
            all: {
                options: {
                    banner: '/**\n * @author\t<%= pkg.author.name %>\n * @data\t<%= grunt.template.today("yyyy.mm.dd") %>\n */\n',
                    restructure: false,
                    report: 'min'
                },
                expand: true,
                cwd: '<%= path.bin %>/<%= path.css %>/',
                src: CONFIG.cssMinFiles,
                dest: '<%= path.release %>/<%= path.css %>/',
                ext: '.min.css'
            }
        },
        //--------------------------------------------------------------
        //  uglify [ https://github.com/gruntjs/grunt-contrib-uglify ]
        //--------------------------------------------------------------
        uglify: {
        },
        //--------------------------------------------------------------
        //  imageoptim [ https://github.com/JamieMason/grunt-imageoptim ]
        //--------------------------------------------------------------
        imageoptim: {
            src: ['bin'],
            options: {
                jpegMini: CONFIG.hasJpegMini,
                imageAlpha: CONFIG.hasImageAlpha,
                quitAfter: true
            }
        },
        //--------------------------------------------------------------
        //  svgmin [ https://github.com/sindresorhus/grunt-svgmin ]
        //--------------------------------------------------------------
        svgmin: {
            options: {
                plugins: [
                    { removeViewBox: false },
                    { removeUselessStrokeAndFill: false }
                ]
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= path.bin %>/img',
                        src: ['**/*.svg'],
                        dest: '<%= path.release %>/img'
                    }
                ]
            }
        },
        //--------------------------------------------------------------
        //  typescript [ https://github.com/k-maru/grunt-typescript ]
        //--------------------------------------------------------------
        typescript: {
            main: {
                options: {
                    module: 'amd', //or commonjs
                    target: 'es5',
                    base_path: '<%= path.src %>/<%= path.ts %>',
                    watch: true,
                    ignoreError: true
                },
                src: '<%= path.src %>/<%= path.ts %>/**/*.ts',
                dest: '<%= path.bin %>/<%= path.js %>/'
            }
        },
        //--------------------------------------------------------------
        //  requirejs [ https://github.com/asciidisco/grunt-requirejs ]
        //--------------------------------------------------------------
        requirejs: {
            main: {
                options: {
                    name: 'main',
                    baseUrl: '<%= path.bin %>/<%= path.js %>/',
                    mainConfigFile: '<%= path.bin %>/<%= path.js %>/main.js',
                    out: '<%= path.bin %>/<%= path.js %>/main.min.js',
                    useStrict: true,
                    preserveLicenseComments: false
                }
            }
        },
        //--------------------------------------------------------------
        //  shared_config [ https://github.com/MathiasPaumgarten/grunt-shared-config ]
        //--------------------------------------------------------------
        shared_config: {
            configAnimate: {
                options: {
                    name: 'ConfigAnimate',
                    cssFormat: 'dash',
                    jsFormat: 'uppercase',
                    amd: false
                },
                src: '<%= path.src %>/config/config_animation.json',
                dest: [
                    '<%= path.src %>/<%= path.sass %>/helpers/_config_animation.scss',
                    '<%= path.bin %>/<%= path.js %>/config-animation.js'
                ]
            }
        },

        //--------------------------------------------------------------
        //  parallelize [ https://github.com/teppeis/grunt-parallelize ]
        //--------------------------------------------------------------
        parallelize: {
        }
    });


    //--------------------------------------------------------------
    // JSファイルを圧縮する
    //--------------------------------------------------------------
    if (CONFIG.jsMinFiles.length > 0) {
        (function () {
            var
                _i = -1,
                _name = CONFIG.jsMinFiles,
                _file,
                _length = _name.length;


            for (; ++_i < _length;) {
                _file = CONFIG.jsMinFiles[_i] + '.js';
                grunt.config.data.uglify[_name[_i]] = {
                    options: {
                        banner: '/**\n * @author\t<%= pkg.author.name %>\n * @data\t<%= grunt.template.today("yyyy.mm.dd") %>\n */\n',
                        mangle: true
                    },
                    expand: true,
                    cwd: '<%= path.bin %>/<%= path.js %>/',
                    src: [_file],
                    dest: '<%= path.release %>/<%= path.js %>/'
                }
                _file = '';
            }
        }());
    }

    //--------------------------------------------------------------
    // sprite画像を生成する
    //--------------------------------------------------------------
    if (CONFIG.sprite.length > 0) {
        (function () {
            var
                _i = -1,
                _spriteTask = CONFIG.sprite,
                _length = _spriteTask.length;

            for (; ++_i < _length;) {
                _spriteTask[_i][1].cssOpts = {'functions': false};
                grunt.config.data.sprite[_spriteTask[_i][0]] = _spriteTask[_i][1];

            }
        }());
    }

    //--------------------------------------------------------------
    // pakage.jsonに記載されているパッケージを自動読み込み
    //--------------------------------------------------------------
    for (taskName in pkg.devDependencies) {
        if (taskName.substring(0, 6) == 'grunt-') {
            grunt.loadNpmTasks(taskName);
        }
    }

    //--------------------------------------------------------------
    // 処理でエラーが出てもgruntを続ける
    //--------------------------------------------------------------
    grunt.registerTask('eatwarnings', function () {
        grunt.warn = grunt.fail.warn = function (warning) {
            grunt.log.error(warning);
        };
    });

    //--------------------------------------------------------------
    // Sassをコンパイルする
    //--------------------------------------------------------------
    grunt.registerTask('default', ['browserSync', 'watch:sass']);

    //--------------------------------------------------------------
    // TSファイルをコンパイル
    //--------------------------------------------------------------
    grunt.registerTask('ts', ['typescript']);

    //--------------------------------------------------------------
    // 画像を圧縮
    //--------------------------------------------------------------
    grunt.registerTask('imgmin', ['imageoptim']);

    //--------------------------------------------------------------
    // 納品ファイルを生成
    //--------------------------------------------------------------
    grunt.registerTask('release', ['clean:release', 'copy:releaseFiles', 'copy:releaseImg', 'htmlmin', 'csso', 'doneRelease']);
    grunt.registerTask('doneRelease', 'bind javascript files.', function () {
        if (CONFIG.hasRequireJs) {
            grunt.task.run('requirejs');
        }
        grunt.task.run('uglify', 'clean:releaseFiles');
    });

};