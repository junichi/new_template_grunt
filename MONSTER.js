// ----------------------------------------------------------------------------
/**
 * Fork from https://bitbucket.org/angedessin/template_grunt
 */
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
/**
 * config.js
 *
 * version 5.1.0
 * update 12/04/14
 *
 * Copyright (c) 2014 Kentaro Otsuka, Junichi Honda All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * ・Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * ・Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * ・Neither the name of the <ORGANIZATION> nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

(function (module) {
    'use strict';
    var _pkg = require('./package.json'),
        MONSTER = {
            projectname: _pkg.name,
            // browserSyncのパスを設定
            // true : bin, false:release
            isDebug: true,
            path: {
                src: 'src',
                bin: 'bin',
                release: 'release',
                js: 'js',
                libs: 'libs',
                css: 'css',
                images: 'images',
                sass: 'sass',
                ts: 'ts'
            },
            files: {
                html: '**/*.html',
                css: '<%= path.css %>/**',
                js: '<%= path.js %>/**',
                images: '<%= dir.images %>/**'
            },
            sassOptions: {
                style: 'expanded',
                sourcemap: 'none'
            },
            // sassファイルへのパス, 出力されるcssファイルのパス
            sassCompilePath: [
                ['/', '<%= path.css %>/']
            ],
            sprite: [
                ['sprite_sample', {
                    imgPath: '../images/sprite_sample.png',
                    src: '<%= path.src %>/sprite/sprite_sample/*.png',
                    destCSS: '<%= path.src %>/<%= path.sass %>/components/sprite/sprite_sample.scss',
                    destImg: '<%= path.bin %>/<%= path.images %>/sprite_sample.png'
                }]
            ],
            cssMinFiles: [
                'style.css'
            ],
            jsMinFiles: [
                'command'
            ],
            notifyMsg: '★Sass Compile!!★',
            // 画像を圧縮するツールの設定
            hasJpegMini: true,
            hasImageAlpha: true,
            // releaseからコピーするファイルを選択
            releaseFiles: [
                'docs/'
            ],
            // releaseから削除するファイルを選択
            deleteFiles: [
                '<%= path.release %>/docs/'
            ],
            bowerFiles: {
                'jquery.min.js': 'jquery/dist/jquery.min.js',
                'require.js': 'requirejs/require.js',
                'lodash.min.js': 'lodash/dist/lodash.min.js',
                'TweenMax.min.js': 'gsap/src/minified/TweenMax.min.js'
            },
            // jsのライブラリFiles
            // これらライブラリをconcatさせるよう
            libs: [
                'bin/js/libs/jquery.min.js',
                'bin/js/libs/lodash.min.js',
                'bin/js/libs/require.min.js',
                'bin/js/libs/TweenMax.min.js'
            ],
            banner: '/*!\n' +
            ' * author: <%= pkg.author.name %> (<%= pkg.author.url %>)\n' +
            ' * version: <%= pkg.version %>\n' +
            ' * Copyright <%= grunt.template.today("yyyy") %> \n' +
            ' */\n',
            isLicenseComment: (function () {
                var licenseRegexp = /^\!|^@preserve|^@cc_on|\bMIT\b|\bMPL\b|\bGPL\b|\(c\)|License|Copyright/mi;
                var _prevCommentLine = 0;
                return function (node, comment) {
                    if (licenseRegexp.test(comment.value) || comment.line === 1 || comment.line === _prevCommentLine + 1) {
                        _prevCommentLine = comment.line;
                        return true;
                    }
                    _prevCommentLine = 0;
                    return false;
                };
            }()),
            sharedConfig: {
                name: 'SAMPLE_CONFIG_ANIMATE',
                cssFormat: 'dash',
                jsFormat: 'uppercase'
            },
            hasRequireJs: false
        };

    module.exports = MONSTER;

}(module));

